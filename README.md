# Endpoints

## From wordpress
- `/projects` - listing all projects
- `/projects/{id}` - details of project - id

- `/posts` - listing all blog posts
- `/posts/{id}` - details of blogs post - id

- `/media?parent=${id}` - getting media associated with the project or post
- `/media/{id}` - getting featured media of the project or post

# Model

## Project model

- id: id_value
- Name: String
- Description: String
- Images: '[String]' // These will be direct links to the image
- FeaturedImage: String // direcct link to image
- Sector: String
- Deliverables(Categories): '[String]' // [Branding, Application Development, Digital Marketing, E-Commerce Integration, Photo and Video Shoot, Website Development]
- Tags: '[String]' // [branding, application, development, ....](all the words from deliverables, but all are words are seperated)

## Blog Model

- Title: String,
- Date: date,
- Author: String,
- Content: Wysiwyg (if possible) otherwise, String