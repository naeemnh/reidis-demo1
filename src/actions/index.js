import api from '../api';
// import history from '../history';
import {
	FETCH_PROJECT,
	FETCH_PROJECTS,
	FETCH_IMAGE,
	FETCH_IMAGES,
	FETCH_FEATURED_IMAGE,
	FETCH_PROJECT_CATEGORIES,
	FETCH_PROJECT_IMAGES,
	FETCH_BLOGS,
	FETCH_BLOG,
	FETCH_SERVICES,
	FETCH_SERVICE,
	FETCH_SLUG_SERVICE,
	FETCH_FAQS,
} from './types';

export const fetchProjects = () => async (dispatch) => {
	const response = await api.get('./projects?orderby=date&order=asc');
	dispatch({ type: FETCH_PROJECTS, payload: response.data });
};
export const fetchProject = (id) => async (dispatch) => {
	const response = await api.get(`/projects/${id}`);
	dispatch({ type: FETCH_PROJECT, payload: response.data });
};
export const fetchProjectCategories = (id) => async (dispatch) => {
	const response = await api.get(`/categories?post=${id}`);
	dispatch({ type: FETCH_PROJECT_CATEGORIES, payload: response.data });
};
export const fetchImages = () => async (dispatch) => {
	const response = await api.get(`./media/`);
	dispatch({ type: FETCH_IMAGES, payload: response.data });
};
export const fetchImage = (id) => async (dispatch) => {
	const response = await api.get(`/media/${id}`);
	dispatch({ type: FETCH_IMAGE, payload: response.data });
};
export const fetchFeaturedImage = (id) => async (dispatch) => {
	const response = await api.get(`/media/${id}`);
	dispatch({ type: FETCH_FEATURED_IMAGE, payload: response.data });
};
export const fetchProjectImages = (id) => async (dispatch) => {
	const response = await api.get(`/media?parent=${id}`);
	dispatch({ type: FETCH_PROJECT_IMAGES, payload: response.data });
};
export const fetchBlogs = () => async (dispatch) => {
	const response = await api.get('/posts');
	dispatch({ type: FETCH_BLOGS, payload: response.data });
};
export const fetchBlog = (id) => async (dispatch) => {
	const response = await api.get(`/posts/${id}`);
	dispatch({ type: FETCH_BLOG, payload: response.data });
};
export const fetchServices = () => async (dispatch) => {
	const response = await api.get('/service');
	dispatch({ type: FETCH_SERVICES, payload: response.data });
};
export const fetchService = (id) => async (dispatch) => {
	const response = await api.get(`/service/${id}`);
	dispatch({ type: FETCH_SERVICE, payload: response.data });
};
export const fetchSlugService = (slug) => async (dispatch) => {
	const response = await api.get(`/service?slug=${slug}`);
	dispatch({ type: FETCH_SLUG_SERVICE, payload: response.data });
};
export const fetchFaqs = () => async (dispatch) => {
	const response = await api.get('/faq');
	dispatch({ type: FETCH_FAQS, payload: response.data });
};
