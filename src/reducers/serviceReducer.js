import { FETCH_SERVICES, FETCH_SERVICE, FETCH_SLUG_SERVICE } from '../actions/types';
import _ from 'lodash';

const serviceReducer = (state = {}, action) => {
    switch (action.type) {
		case FETCH_SERVICES:
			return { ...state, ..._.mapKeys(action.payload, 'id') };
		case FETCH_SERVICE:
			return { ...state, [action.payload.id]: action.payload };
        case FETCH_SLUG_SERVICE:
            return {...state, [action.payload.id]: action.payload };
		default:
			return state;
	}
}

export default serviceReducer;
