import { FETCH_PROJECT_CATEGORIES } from '../actions/types';
import _ from 'lodash';

const categoryReducer = (state = {}, action) => {
	switch (action.type) {
		case FETCH_PROJECT_CATEGORIES:
			return { ...state, ..._.mapKeys(action.payload, 'id') };
		default:
			return state;
	}
};

export default categoryReducer;
