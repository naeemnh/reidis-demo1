import {
	FETCH_IMAGE,
	FETCH_IMAGES,
	FETCH_FEATURED_IMAGE,
	FETCH_PROJECT_IMAGES
} from '../actions/types';

import _ from 'lodash';

const imageReducer = (state = {}, action) => {
	switch (action.type) {
		case FETCH_IMAGES:
			return { ...state, ..._.mapKeys(action.payload, 'id') };
		case FETCH_FEATURED_IMAGE:
			return { ...state, [action.payload.id]: action.payload };
		case FETCH_IMAGE:
			return { ...state, [action.payload.id]: action.payload };
		case FETCH_PROJECT_IMAGES:
			return { ...state, ..._.mapKeys(action.payload, 'id') }
		default:
			return state;
	}
};

export default imageReducer;
