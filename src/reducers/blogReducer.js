import { FETCH_BLOG, FETCH_BLOGS } from '../actions/types';

import _ from 'lodash';

const blogReducer = (state = {}, action) => {
	switch (action.type) {
		case FETCH_BLOGS:
			return { ...state, ..._.mapKeys(action.payload, 'id') };
		case FETCH_BLOG:
			return { ...state, [action.payload.id]: action.payload };
		default:
			return state;
	}
};

export default blogReducer;