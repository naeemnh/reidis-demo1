import { FETCH_FAQS } from '../actions/types';
import _ from 'lodash';

const faqReducer = (state = {}, action) => {
	switch (action.type) {
		case FETCH_FAQS:
			return { ...state, ..._.mapKeys(action.payload, 'id') };
		default:
			return state;
	}
};

export default faqReducer;
