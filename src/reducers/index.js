import { combineReducers } from 'redux';
import imageReducer from './imageReducer';
import projectReducer from './projectReducer';
import blogReducer from './blogReducer';
import categoryReducer from './categoryReducer';
import serviceReducer from './serviceReducer';
import faqReducer from './faqReducer';

export default combineReducers({
	categories: categoryReducer,
	services: serviceReducer,
	projects: projectReducer,
	images: imageReducer,
	blogs: blogReducer,
	faqs: faqReducer,
});
