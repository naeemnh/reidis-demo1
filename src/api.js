import axios from 'axios';

export default axios.create({
	baseURL: 'https://reidis.website/reidis-demo-backend/wp-json/wp/v2',
});
