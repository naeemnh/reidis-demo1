import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { fetchImage } from '../actions';

const FeaturedImage = (props) => {
	useEffect(() => {
		const { imgId } = props;
		if (imgId !== 0) props.fetchImage(imgId);
	}, []);
	if (!props.image) return <img src="/images/img1.png" alt="..." />;
	return <img src={props.image} alt="..." />;
};

const mapStateToProps = (state, { imgId }) => {
	if (!state.images[imgId]) return { image: '/images/img1.png' };
	return { image: state.images[imgId].source_url };
};

export default connect(mapStateToProps, { fetchImage })(FeaturedImage);
