import React, { Fragment } from 'react';

import { ContactSection, TeamSection } from './Partials';

const Home = () => {
	// ========================Main Page Animation Content========================
	const content =
		'Front-end Development Marketing SEO Digital Products Wordpress Development Branding Web Application Development eCommerce Development Digital Brand Search Management Search Engine Optimization Social Media Marketing Instagram Facebook Ads Payment Integration UI/UX Design Google Adwords App Store Optimization Content Writing';

	const words = content.split(' ');

	const shuffle = (arr) => {
		var ci = arr.length,
			ri;
		while (ci !== 0) {
			ri = Math.floor(Math.random() * ci);
			ci--;

			[arr[ci], arr[ri]] = [arr[ri], arr[ci]];
		}
		return arr;
	};

	const loadHeroContent = shuffle(words).map((word) => {
		return `${word} `;
	});
	return (
		<Fragment>
			<header id="home" className="container-fluid header">
				<div className="slide-text">
					<div className="unreal">{loadHeroContent}</div>
					<div className="unreal">{loadHeroContent}</div>
					<div className="unreal">{loadHeroContent}</div>
					<div className="unreal">{loadHeroContent}</div>
					<div className="unreal">{loadHeroContent}</div>
					<div className="unreal">{loadHeroContent}</div>
					<div className="unreal">{loadHeroContent}</div>
				</div>
				<div className="hero-text">
					<h3>
						team <br />
						towards <br />
						success
					</h3>
					<p>
						<a href="#about-header">
							scroll down <i className="play-button"></i>
						</a>
					</p>
				</div>
				<img
					src="https://reidis.website/reidis-demo-backend/wp-content/uploads/2021/10/Redies_Smiley-Man.png"
					alt=""
				/>
				<div className="hero-text">
					<h3>
						success
						<br />
						stories
					</h3>
					<p>
						<a href="#portfolio">
							portfolio <i className="icon-arrow"></i>
						</a>
					</p>
				</div>

				{/* <div className="video-filter"></div>
				<div className="mouse-scroll"></div> */}
			</header>
			<section id="about-header" className="container-fluid about-header">
				<div className="row">
					<div className="col">
						<div className="lg-text">
							<span>INSPIRED</span>
							<span>
								CREATIVE{' '}
								<svg viewbox="0,0 100,100" width="200px" height="200px">
									{/* the path to be animated along */}
									<path
										class="track"
										fill="none"
										stroke-width="0.25"
										d="M 25, 50
											a 25,25 0 1,1 50,0
											a 25,25 0 1,1 -50,0"
									/>

									{/* the mover */}
									<circle class="marker" r="10" fill="#e71d25"></circle>
								</svg>
							</span>
							<span>functional</span>
						</div>
						<div className="normal-text">
							<p>
								Hello, world! We are Reidis, a new age Interactive agency that
								brings together brand custodians, gifted designers, creative
								developers, and imaginative writers from across the world with a
								unified vision to build brands with global resonance.
							</p>
						</div>
					</div>
				</div>
			</section>
			{/* Website Development */}
			<div id="portfolio" className="container-fluid box-content">
				<div className="row">
					<div className="col-md-6">
						<div className="boxy c2-color">
							<div className="row">
								<div className="col">
									<h1 className="title">
										WEBSITE
										<br />
										DEVELOPMENT
									</h1>
								</div>
							</div>
							<div className="row">
								<div className="col-xs-12 col-sm-6 col-md-12 col-lg-5">
									<div className="text">
										<ul>
											<li>Digital Content</li>
											<li>Interfaces</li>
											<li>Product Design</li>
										</ul>
									</div>
								</div>
								<div className="col-xs-12 col-sm-6 col-md-12 col-lg-7">
									<div className="text">
										<ul>
											<li>User Experience</li>
											<li>Interactivity</li>
											<li>Content Production</li>
											<li>Checkout Optimization</li>
										</ul>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div className="col-md-6">
						<div className="boxy img-box">
							<div className="img">
								<img
									src="https://reidis.website/reidis-demo-backend/wp-content/uploads/2021/11/Phone.png"
									alt=""
								/>
							</div>
							<div className="bottom-text">
								<div className="link">VIEW THIS PROJECT</div>
								<div className="text">UIUX Design for Alhula</div>
							</div>
							<a href="/works/77" className="project-link-full"></a>
						</div>
					</div>
				</div>
			</div>
			{/* LOGO MARKS AND BRANDING */}
			<div className="container-fluid box-content">
				<div className="row flex-row-reverse">
					<div className="col-md-6">
						<div className="boxy c1-color">
							<div className="row">
								<div className="col">
									<h1 className="title">
										LOGO MARKS &<br />
										BRANDING
									</h1>
								</div>
							</div>
							<div className="row">
								<div className="col-xs-12 col-sm-6 col-md-12 col-lg-5">
									<div className="text">
										<ul>
											<li>Logo Marks</li>
											<li>Brand Strategy</li>
											<li>Design Systems</li>
										</ul>
									</div>
								</div>
								<div className="col-xs-12 col-sm-6 col-md-12 col-lg-7">
									<div className="text">
										<ul>
											<li>Brand Identity</li>
											<li>Brand Architecture</li>
											<li>Naming & Verbal Identity</li>
											<li>Launch & Brand Campaigns</li>
										</ul>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div className="col-md-6">
						<div className="boxy img-box">
							<div className="img">
								<img
									src="https://reidis.website/reidis-demo-backend/wp-content/uploads/2021/10/WEB_02.png"
									alt=""
								/>
							</div>
							<div className="bottom-text">
								<div className="link">View this project</div>
								<div className="text">Branding for Simplify</div>
							</div>
							<a href="/services/212" className="project-link-full"></a>
						</div>
					</div>
				</div>
			</div>
			{/* Ecommerce Integration */}
			<div className="container-fluid box-content">
				<div className="row">
					<div className="col-md-6">
						<div className="boxy default-color">
							<div className="row">
								<div className="col">
									<h1 className="title">
										E-COMMERCE
										<br />
										INTEGRATION
									</h1>
								</div>
							</div>
							<div className="row">
								<div className="col-xs-12 col-sm-6 col-md-12 col-lg-5">
									<div className="text">
										<ul>
											<li>Design of Mockups</li>
											<li>Navigation flows.</li>
											<li>Design Content development</li>
											<li>Quick buying Strategies</li>
										</ul>
									</div>
								</div>
								<div className="col-xs-12 col-sm-6 col-md-12 col-lg-7">
									<div className="text">
										<ul>
											<li>Programming in Woocommerce</li>
											<li></li>
											<li></li>
										</ul>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div className="col-md-6">
						<div className="boxy img-box">
							<div className="img">
								<img
									src="https://reidis.website/reidis-demo-backend/wp-content/uploads/2021/11/Symplify.png"
									alt=""
								/>
							</div>
							<div className="bottom-text">
								<div className="link">VIEW THIS PROJECT</div>
								<div className="text">
									E-Commerce Website for The Baby Boutique
								</div>
							</div>
							<a href="/works/77" className="project-link-full"></a>
						</div>
					</div>
				</div>
			</div>
			{/* Digital Marketing */}
			<div className="container-fluid box-content">
				<div className="row flex-lg-row-reverse">
					<div className="col-md-6">
						<div className="boxy c1-color">
							<div className="row">
								<div className="col">
									<h1 className="title">DIGITAL MARKETING</h1>
								</div>
							</div>
							<div className="row">
								<div className="col-xs-12 col-sm-6 col-md-12 col-lg-5">
									<div className="text">
										<ul>
											<li>Packaging Design</li>
											<li>Retail Design</li>
											<li>Interior Design</li>
											<li>Print Design</li>
										</ul>
									</div>
								</div>
								<div className="col-xs-12 col-sm-6 col-md-12 col-lg-7">
									<div className="text">
										<ul>
											<li>Stationery Design</li>
											<li>Printing & Production</li>
											<li>Gift Items</li>
										</ul>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div className="col-md-6">
						<div className="boxy img-box">
							<div className="img">
								<img
									src="https://reidis.website/reidis-demo-backend/wp-content/uploads/2021/09/web_icon_flyxpo.png"
									alt=""
								/>
							</div>
							<div className="bottom-text">
								<div className="link">VIEW THIS PROJECT</div>
								<div className="text">Marketing Campaign for Flyxpo</div>
							</div>
							<a href="/works" className="project-link-full"></a>
						</div>
					</div>
				</div>
			</div>
			{/* Application Development */}
			<div className="container-fluid box-content">
				<div className="row">
					<div className="col-md-6">
						<div className="boxy default-color">
							<div className="row">
								<div className="col">
									<h1 className="title">
										APPLICATION
										<br />
										DEVELOPMENT
									</h1>
								</div>
							</div>
							<div className="row">
								<div className="col-12">
									<div className="text">
										<ul>
											<li>Platform migrations</li>
											<li>HTML5 & CSS</li>
											<li>GULP / VUE.JS FRAMEWORKS</li>
											<li>RUBY ON RAILS</li>
										</ul>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div className="col-md-6">
						<div className="boxy img-box">
							<div className="img">
								<img
									src="https://reidis.website/reidis-demo-backend/wp-content/uploads/2021/10/04.png"
									alt=""
								/>
							</div>
							<div className="bottom-text">
								<div className="link">VIEW THIS PROJECT</div>
								<div className="text">AlFriday Lighting</div>
							</div>
							<a href="/works" className="project-link-full"></a>
						</div>
					</div>
				</div>
			</div>
			{/* Photo and Video Shoot */}
			<div className="container-fluid box-content">
				<div className="row flex-lg-row-reverse">
					<div className="col-md-6">
						<div className="boxy c1-color">
							<div className="row">
								<div className="col">
									<h1 className="title">
										PHOTO & <br />
										VIDEO SHOOT
									</h1>
								</div>
							</div>
							<div className="row">
								<div className="col-xs-12 col-sm-6 col-md-12 col-lg-5">
									<div className="text">
										<ul>
											<li>PACKAGing design</li>
											<li>Retail design</li>
											<li>Interior design</li>
											<li>PRINT DESIGN</li>
										</ul>
									</div>
								</div>
								<div className="col-xs-12 col-sm-6 col-md-12 col-lg-7">
									<div className="text">
										<ul>
											<li>STATIONERY DESIGN</li>
											<li>printing & production</li>
											<li>GIFT ITEMS</li>
										</ul>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div className="col-md-6">
						<div className="boxy img-box">
							<div className="img">
								<video
									id="videoId"
									src="/videos/video-service.mp4"
									autoPlay
									loop
									muted></video>
								<video autoPlay loop muted>
									<source src="/videos/video-service.webm" type="video/webm" />
									<source src="/videos/video-service.mp4" type="video/mp4" />
								</video>
							</div>
							<div className="bottom-text">
								<div className="link">VIEW THIS PROJECT</div>
								<div className="text">Video shoot for Decopex Interiors</div>
							</div>
							<a href="/works" className="project-link-full"></a>
						</div>
					</div>
				</div>
			</div>
			{/* <AboutSection /> */}
			<TeamSection />
			<ContactSection />
		</Fragment>
	);
};

export default Home;
