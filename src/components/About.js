import React, { Fragment } from 'react';
// import { Link } from 'react-router-dom';

import { ContactSection, TeamSection } from './Partials';

const About = () => {
	return (
		<Fragment>
			<header className="container-fluid blog-header">
				<div className="row"></div>
			</header>
			<header className="container-fluid about-header">
				<div className="row">
					<div className="col">
						<div className="lg-text">
							<span>INSPIRED.</span>
							<br />
							<span>CREATIVE.</span>
							<br />
							<span>FUNCTIONAL.</span>
						</div>
						<div className="normal-text">
							<p>
								Hello, world! We are Reidis, a new age Interactive agency that
								brings together brand custodians, gifted designers, creative
								developers, and imaginative writers from across the world with a
								unified vision to build brands with global resonance. At Reidis,
								we don’t just provide solutions across mediums but deliver
								thought processes that integrate branding and marketing to
								ensure maximum visibility and conversions.
							</p>
						</div>
						<img
							src="/images/Icon_01.png"
							alt="open-quote"
							className="open-quote"
						/>
						<img
							src="/images/Icon_02.png"
							alt="close-quote"
							className="close-quote"
						/>
					</div>
				</div>
			</header>
			<TeamSection />
			<div className="container-fluid process-section">
				<div className="row">
					<div className="col">
						<div className="lg-text">
							<span className="other-color">our process</span>
						</div>
						<div className="normal-text">
							<p>
								But I must explain to you how all this mistaken idea of
								denouncing pleasure and praising
								<br />
								pain was born and I will give you a complete account of the
								system.
							</p>
						</div>
					</div>
				</div>
				<div className="row">
					<div className="col-md-4">
						<div className="text-box">
							<div className="title">Sketch & Drawing</div>
							<p>
								But I must explain to you how all this mistaken idea of
								denouncing pleasure and praising pain was born and I will give
								you a complete account of the
							</p>
						</div>
					</div>
					<div className="col-md-4">
						<div className="text-box">
							<div className="title">Designing</div>
							<p>
								But I must explain to you how all this mistaken idea of
								denouncing pleasure and praising pain was born and I will give
								you a complete account of the
							</p>
						</div>
					</div>
					<div className="col-md-4">
						<div className="text-box">
							<div className="title">Development</div>
							<p>
								But I must explain to you how all this mistaken idea of
								denouncing pleasure and praising pain was born and I will give
								you a complete account of the
							</p>
						</div>
					</div>
				</div>
			</div>
			<div className="container-fluid jobs-section padding-for-team">
				<div className="row">
					<div className="col">
						<div className="lg-text">
							<span className="other-color">job openings</span>
						</div>
					</div>
				</div>
				<div className="row job-box-row">
					<div className="col-sm-7">
						<div className="job-box">
							<div className="title">Frontend developer</div>
							<div className="subtitle">Remote</div>
						</div>
					</div>
					<div className="col-sm-5">
						<div className="btn-holder">
							<a href="/" className="cr-btn black ex-padding">
								Apply Now
							</a>
						</div>
					</div>
				</div>
				<div className="row job-box-row">
					<div className="col-sm-7">
						<div className="job-box">
							<div className="title">UX Researcher</div>
							<div className="subtitle">Remote</div>
						</div>
					</div>
					<div className="col-sm-5">
						<div className="btn-holder">
							<a href="/" className="cr-btn black ex-padding">
								Apply Now
							</a>
						</div>
					</div>
				</div>
				<div className="row job-box-row">
					<div className="col-sm-7">
						<div className="job-box">
							<div className="title">UI Designer</div>
							<div className="subtitle">DUBAI, UAE</div>
						</div>
					</div>
					<div className="col-sm-5">
						<div className="btn-holder">
							<a href="/" className="cr-btn black ex-padding">
								Apply Now
							</a>
						</div>
					</div>
				</div>
				<div className="row job-box-row">
					<div className="col-sm-7">
						<div className="job-box">
							<div className="title">Client service agent</div>
							<div className="subtitle">DUBAI, UAE</div>
						</div>
					</div>
					<div className="col-sm-5">
						<div className="btn-holder">
							<a href="/" className="cr-btn black ex-padding">
								Apply Now
							</a>
						</div>
					</div>
				</div>
			</div>
			<ContactSection />
		</Fragment>
	);
};

export default About;
