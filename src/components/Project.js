import React, { Fragment, useEffect } from 'react';
import { connect } from 'react-redux';
import { ContactSection } from './Partials';
import {
	fetchProject,
	fetchProjectCategories,
	fetchProjectImages,
} from '../actions';

const Project = (props) => {
	const { project, projectCategories, images } = props;
	const { id } = props.match.params;
	useEffect(() => {
		props.fetchProject(id);
		props.fetchProjectCategories(id);
		props.fetchProjectImages(id);
	}, []);
	const renderCategories = projectCategories.map((category) => (
		<Fragment key={category.id}>
			{category.name} <br />
		</Fragment>
	));
	const renderImages = images.map((image) => (
		<div className="img-holder" key={image.id}>
			<img src={image.source_url} alt="" />
		</div>
	));
	return (
		<Fragment>
			<header className="container-fluid project-header">
				<div className="row"></div>
			</header>
			<div className="container-fluid project-info">
				<div className="row">
					<div className="col">
						<h2 className="lg-text">
							{project ? project.title.rendered : null}
						</h2>
						<div className="normal-text">
							<p>{project ? project.acf.description : null}</p>
						</div>
					</div>
				</div>
				<div className="row">
					<div className="col-sm-6 col-md-3">
						<div className="text-box">
							<div className="title">Client</div>
							<p>{project ? project.title.rendered : null}</p>
						</div>
					</div>
					<div className="col-sm-6 col-md-3">
						<div className="text-box">
							<div className="title">Website</div>
							<p>{project ? project.acf.website : null}</p>
						</div>
					</div>
					<div className="col-sm-6 col-md-3">
						<div className="text-box">
							<div className="title">Sector</div>
							<p>{project ? project.acf.sector : null}</p>
						</div>
					</div>
					<div className="col-sm-6 col-md-3">
						<div className="text-box">
							<div className="title">Deliverables</div>
							<p>{projectCategories ? renderCategories : null}</p>
						</div>
					</div>
				</div>
			</div>
			<div className="container project-imgs">
				<div className="row">
					<div className="col">{renderImages}</div>
				</div>
			</div>
			<ContactSection />
		</Fragment>
	);
};

const mapStateToProps = (state, ownProps) => {
	console.log(state);
	return {
		project: state.projects[ownProps.match.params.id],
		projectCategories: Object.values(state.categories),
		images: Object.values(state.images),
	};
};

export default connect(mapStateToProps, {
	fetchProject,
	fetchProjectCategories,
	fetchProjectImages,
})(Project);
