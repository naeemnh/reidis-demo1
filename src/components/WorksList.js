import React, { Fragment, useEffect } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';

import { fetchProjects } from '../actions';
import { ContactSection } from './Partials';
import FeaturedImage from './FeaturedImage';

const WorksList = (props) => {
	const { projects } = props;
	useEffect(() => {
		props.fetchProjects();
	}, []);
	const renderList = projects.map((project) => (
		<div className="col-md-6" key={project.id}>
			<div className="boxy img-box">
				<div className="img">
					<FeaturedImage imgId={project.featured_media} />
				</div>
				<div className="bottom-text">
					<div className="link">VIEW THIS PROJECT</div>
					<div className="text">{project.title.rendered}</div>
				</div>
				<Link to={`/works/${project.id}`} className="project-link-full"></Link>
			</div>
		</div>
	));
	return (
		<Fragment>
			<header className="container-fluid header">
				<div className="mouse-scroll"></div>
				<div className="row">
					<div className="col">
						<div className="extra-lg-text">
							<span>perfection is</span>
							<br />
							<span>not a myth</span>
							<br />
							<span className="other-color">check our</span>
							<br />
							<span className="other-color">work.</span>
						</div>
					</div>
				</div>
			</header>

			<div className="container-fluid box-content">
				<div className="row">{renderList}</div>
			</div>
			<ContactSection />
		</Fragment>
	);
};

const mapStateToProps = (state) => {
	return {
		projects: Object.values(state.projects),
	};
};

export default connect(mapStateToProps, { fetchProjects })(WorksList);
