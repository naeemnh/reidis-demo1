import React from 'react';
// import { Link } from 'react-router-dom';

const Footer = () => {
	return (
		<footer className="container-fluid footer">
			<div className="row">
				<div className="col">
					<div className="lg-text">
						<span>100% satisfication.</span>
						<br />
						<span>let’s create</span>
					</div>
					<div className="normal-text">
						<p>
							We’ll take your business to the next level, with our proven
							<br />
							strategies, latest technologies and friendly creatives that
							<br />
							will work to produce the best outcome possible.
						</p>
					</div>
				</div>
			</div>
			<div className="row">
				<div className="col">
					<div className="contact-info-holder">
						<div className="title">Call us</div>
						<div className="contact-info">+971 52 727 6542</div>
					</div>
				</div>
				<div className="col">
					<div className="contact-info-holder">
						<div className="title">E-mail</div>
						<div className="contact-info">
							<a href="mailto:team@reidis.io">
								<span
									className="__cf_email__"
									data-cfemail="026a676e6e6d426170673a2c616d6f">
									team@reidis.io
								</span>
							</a>
						</div>
						<div className="social-media">
							<div className="social-link-holder">
								<a href="/">Facebook</a>
							</div>
							<div className="social-link-holder">
								<a href="/">Instagram</a>
							</div>
							<div className="social-link-holder">
								<a href="/">LinkedIn</a>
							</div>
							<div className="social-link-holder">
								<a href="/">Behance</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</footer>
	);
};

export default Footer;
