import React from 'react';

const FooterB = () => {
	return (
		<footer className="container-fluid footer">
			<div className="row">
				<div className="col-md-3 footer-main-menu">
					<ul>
						<li>
							<a href="/">Home</a>
						</li>
						<li>
							<a href="/about">About Us</a>
						</li>
						<li>
							<a href="/works">Works</a>
						</li>
						<li>
							<a href="/services/212">Services</a>
						</li>
						<li>
							<a href="/contact">Contact Us</a>
						</li>
					</ul>
				</div>
				<div className="col-md-3">
					<ul>
						<li>
							<a href="#">Design</a>
						</li>
						<li>
							<a href="#">Branding</a>
						</li>
						<li>
							<a href="#">Logo Design</a>
						</li>
						<li>
							<a href="#">Brochures & Pamphlets</a>
						</li>
					</ul>
				</div>
				<div className="col-md-3">
					<ul>
						<li>
							<a href="#">Development</a>
						</li>
						<li>
							<a href="#">Web Design</a>
						</li>
						<li>
							<a href="#">Web Development</a>
						</li>
						<li>
							<a href="#">Application Development</a>
						</li>
						<li>
							<a href="#">E-Commerce Integration</a>
						</li>
						<li>
							<a href="#">Web Domain & Hosting</a>
						</li>
					</ul>
				</div>
				<div className="col-md-3">
					<ul>
						<li>
							<a href="#">Marketing</a>
						</li>
						<li>
							<a href="#">Social Media Campaigns</a>
						</li>
						<li>
							<a href="#">Photo & Video Shooting</a>
						</li>
						<li>
							<a href="#">Content Writing</a>
						</li>
						<li>
							<a href="#">Digital Marketing</a>
						</li>
						<li>
							<a href="#">Ads</a>
						</li>
					</ul>
				</div>
			</div>
			<div className="row">
				<div className="col footer-socials">
					<ul>
						<li>
							<a href="https://www.facebook.com/reidisinteractive">Facebook</a>
						</li>
						<li>
							<a href="https://www.linkedin.com/company/reidis-interactive/mycompany/">
								LinkedIn
							</a>
						</li>
						<li>
							<a href="https://instagram.com/reidis_io?utm_medium=copy_link">
								Instagram
							</a>
						</li>
						<li>
							<a href="https://www.behance.net/reidisinteractive">Behance</a>
						</li>
					</ul>
				</div>
			</div>
			<div className="row">
				<div className="col disclaimer">
					<div className="normal-text">
						<p>© 2021 Reidis Interactive. All Rights Reserved</p>
					</div>
				</div>
			</div>
		</footer>
	);
};

export default FooterB;
