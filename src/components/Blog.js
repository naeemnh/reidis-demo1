import React, { Component, Fragment, useEffect } from 'react';
import { connect } from 'react-redux';
import { fetchBlog } from '../actions';
import PageNotFound from './PageNotFound';
import moment from 'moment';

import FeaturedImage from './FeaturedImage';

const Blog = (props) => {
	useEffect(() => {
		const { id } = props.match.params;
		props.fetchBlog(id);
		console.log(props);
	}, []);
	const { blog } = props;
	return (
		<Fragment>
			<header className="container-fluid blog-header">
				<div className="row"></div>
			</header>
			<div className="container post-section">
				<div className="row">
					<div className="col">
						<div className="post-header">
							<div className="text-holder">
								<div className="post-info-holder">
									<div className="link-holder">
										<a href="/blog">&#x3C; Back</a>
									</div>
									<div className="post-info">
										<div className="date">
											{blog ? moment(blog.date).format('MMM Do, YYYY') : null}
										</div>
										<div className="auther">by Julia Marcus</div>
									</div>
								</div>
								<div className="title">{blog ? blog.title.rendered : null}</div>
								<div className="text">
									{/* But I must explain to you how all this mistaken idea of
										denouncing pleasure and praising pain was born and I will
										give you a complete account of the system. */}
									{blog ? blog.acf.post_info : null}
								</div>
							</div>
							<div className="img-holder">
								<FeaturedImage src={blog ? blog.featured_media : null} />
							</div>
						</div>
						<div
							className="post-content"
							dangerouslySetInnerHTML={{
								__html: blog ? blog.content.rendered : null,
							}}></div>
					</div>
				</div>
			</div>
		</Fragment>
	);
};

const mapStateToProps = (state, ownProps) => {
	return { blog: state.blogs[ownProps.match.params.id] };
};

export default connect(mapStateToProps, { fetchBlog })(Blog);
