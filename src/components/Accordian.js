import React, { Fragment, useEffect, useState, useRef } from 'react';
import { connect } from 'react-redux';
import { fetchFaqs } from '../actions';

const Accordian = (props) => {
	const [clicked, setClicked] = useState('0');
	const { faqs } = props;

	useEffect(() => {
		props.fetchFaqs();
	}, []);

	const onQuestionClick = (index) => {
		if (clicked === index) return setClicked('0');
		setClicked(index);
	};

	const contentEl = useRef();

	// const renderQuestions = faqs.map((question) => {
	// 	return (
	// 		<Fragment key={question.id}>
	// 			<button
	// 				className={`accordion ${active}`}
	// 				onClick={() => {
	// 					onQuestionClick(question.id);
	// 				}}>
	// 				<i className="fas fa-plus"></i>
	// 				{question.title.rendered}
	// 			</button>
	// 			<div className={`panel ${active}`}>
	// 				<p>{question.acf.answer}</p>
	// 			</div>
	// 		</Fragment>
	// 	);
	// });

	return faqs.map((question) => {
		const active = clicked === question.id;

		return (
			<Fragment key={question.id}>
				<button
					className={`accordion ${active ? 'active' : ''}`}
					onClick={() => {
						onQuestionClick(question.id);
					}}>
					<i className={`fas ${active ? 'fa-minus' : 'fa-plus'}`}></i>
					{question.title.rendered}
				</button>
				<div
					ref={contentEl}
					className="panel"
					style={
						active
							? { height: contentEl.current.scrollHeight + 50 }
							: { height: '0px' }
					}>
					<p>{question.acf.answer}</p>
				</div>
			</Fragment>
		);
	});
};

const mapStateToProps = (state) => {
	return {
		faqs: Object.values(state.faqs),
	};
};

export default connect(mapStateToProps, { fetchFaqs })(Accordian);
