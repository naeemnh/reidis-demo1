import React, { Fragment } from 'react';

const PageNotFound = () => {
	return (
		<Fragment>
			<header className="container-fluid header">
				<div className="row">
					<div className="col">
						<div className="extra-lg-text">
							<span className="other-color">404 Page Not Found</span>
							{/* <!-- <span className="other-color">Services</span> --> */}
						</div>
					</div>
				</div>
			</header>
		</Fragment>
	);
};

export default PageNotFound;
