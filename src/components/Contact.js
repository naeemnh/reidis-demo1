import React, { Fragment } from 'react';

const Contact = () => {
	return (
		<Fragment>
			<header id="contact" className="container-fluid header">
				<div className="row">
					<div className="col">
						<div className="lg-text">
							<span>Get in touch</span>
							{/* <br />
							<span>amazing things</span>
							<br />
							<span>together</span> */}
						</div>
						<div className="normal-text">
							<p>
								Need help with an amibitous project?
								<br />
								Contact us to present you idea.
							</p>
						</div>
					</div>
				</div>
			</header>

			<div className="container-fluid box-content">
				<div className="row">
					<div className="col-md-6">
						<div className="boxy simple-data c1-color">
							<div className="slg-text">
								<span>visit us</span>
							</div>
							<div className="normal-lg-text">
								<p>
									We’re in Dubai, UAE
									<br />
									Burjman, Bank Street
									<br />
									Al Abbas Bldg, FLR 6, #12
								</p>
							</div>
						</div>
					</div>
					<div className="col-md-6">
						<div className="boxy simple-data primary-color">
							<div className="slg-text mb-0">
								<span>call us</span>
							</div>
							<div className="normal-lg-text">
								<p>+971 52 727 6542</p>
							</div>
							<div className="container v-space-5"></div>
							<div className="slg-text mb-0">
								<span>Email Us</span>
							</div>
							<div className="normal-lg-text">
								<p>
									Tell us about your awesome
									<br />
									project, or just say hi at
									<br />
									<a
										href="mailto:team@reidis.io"
										className="__cf_email__"
										data-cfemail="90f8f5fcfcffd0f3e2f5a8bef3fffd">
										team@reidis.io
									</a>
								</p>
							</div>
						</div>
					</div>
				</div>
			</div>
			{/* <div className="container-fluid our-people-section">
				<div className="row">
					<div className="col">
						<div className="extra-lg-text">
							<span>behind every</span>
							<br />
							<span>great project,</span>
							<br />
							<span>reidis team!</span>
						</div>
						<div className="lg-text">
							<span className="other-color">Have an Idea? </span>
						</div>
						<div className="normal-text">
							<p>
								Need help with an ambitious project?
								<br /> Contact us to present your idea.
							</p>
						</div>
					</div>
				</div>
				<div className="row people-box-row">
					<div className="col-sm-7">
						<div className="people-box">
							<div className="title">Jithin Nivas</div>
							<div className="subtitle">Digital Consultant</div>
						</div>
					</div>
					<div className="col-sm-5">
						<div className="btn-holder">
							<a
								href="mailto:jithin@reidis.io"
								className="cr-btn black ex-padding">
								<span
									className="__cf_email__"
									data-cfemail="214b544d484061425344190f424e4c">
									[email&#160;protected]
								</span>
							</a>
						</div>
					</div>
				</div>
				<div className="row people-box-row">
					<div className="col-sm-7">
						<div className="people-box">
							<div className="title">laila durazi</div>
							<div className="subtitle">Creative Director / UIUX & CEO</div>
						</div>
					</div>
					<div className="col-sm-5">
						<div className="btn-holder">
							<a href="#" className="cr-btn black ex-padding">
								<span
									className="__cf_email__"
									data-cfemail="ed818c84818cad8e9f88d5c38e8280">
									[email&#160;protected]
								</span>
							</a>
						</div>
					</div>
				</div>
				<div className="row people-box-row">
					<div className="col-sm-7">
						<div className="people-box">
							<div className="title">faisal a.</div>
							<div className="subtitle">Frontend Developer, CEO</div>
						</div>
					</div>
					<div className="col-sm-5">
						<div className="btn-holder">
							<a href="#" className="cr-btn black ex-padding">
								<span
									className="__cf_email__"
									data-cfemail="8ceaede5ffede0cceffee9b4a2efe3e1">
									[email&#160;protected]
								</span>
							</a>
						</div>
					</div>
				</div>
			</div> */}
		</Fragment>
	);
};

export default Contact;
