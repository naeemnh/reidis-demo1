import React, { Fragment, Component, useEffect } from 'react';
import { connect } from 'react-redux';
// import { Link } from 'react-router-dom';
import { fetchBlogs } from '../actions';

const BlogList = (props) => {
	useEffect(() => {
		props.fetchBlogs();
	}, []);
	const renderList = props.blogs.map((blog) => {
		return (
			<div className="post-box" key={blog.id}>
				<div className="text-holder">
					<a href={`/blog/${blog.id}`} className="title">
						{blog.title.rendered}
					</a>
					<div
						className="text"
						dangerouslySetInnerHTML={{
							__html: `${blog.excerpt.rendered}`,
						}}></div>
				</div>
				<div className="img-holder">
					<img src="images/post1.png" alt="" />
				</div>
			</div>
		);
	});
	return (
		<Fragment>
			<header className="container-fluid header">
				<div className="row">
					<div className="col">
						<div className="lg-text">
							<span>PRODUCT TIPS</span>
							<br />
							<span>DESIGN & BUSINESS</span>
							<br />
							<span className="other-color">cre8 blog</span>
						</div>
						<div className="normal-text">
							<p>
								You can call it an extra arm that support you with insightful
								ideas,
								<br />
								about business, design, productivity, design or even personal
								<br />
								development for business people.
							</p>
						</div>
					</div>
				</div>
			</header>
			<div className="container-fluid blog-section">
				<div className="row">
					<div className="col">{renderList}</div>
				</div>
			</div>
		</Fragment>
	);
};

// class BlogList extends Component {
// 	componentDidMount() {
// 		this.props.fetchBlogs();
// 		console.log(this.props);
// 	}
// 	renderList() {
// 		return this.props.blogs.map((blog) =>{
// 			return (
// 				<div className="post-box" key={blog.id}>
// 					<div className="text-holder">
// 						<a href={`/blog/${blog.id}`} className="title">
// 							{blog.title.rendered}
// 						</a>
// 						<div className="text" dangerouslySetInnerHTML={{__html: `${blog.excerpt.rendered}`}}>
// 						</div>
// 					</div>
// 					<div className="img-holder">
// 						<img src="images/post1.png" alt="" />
// 					</div>
// 				</div>
// 			)
// 		})
// 	}
// 	render() {
// 		return (
// 			<Fragment>
// 				<header className="container-fluid header">
// 					<div className="row">
// 						<div className="col">
// 							<div className="lg-text">
// 								<span>PRODUCT TIPS</span>
// 								<br />
// 								<span>DESIGN & BUSINESS</span>
// 								<br />
// 								<span className="other-color">cre8 blog</span>
// 							</div>
// 							<div className="normal-text">
// 								<p>
// 									You can call it an extra arm that support you with insightful
// 									ideas,
// 									<br />
// 									about business, design, productivity, design or even personal
// 									<br />
// 									development for business people.
// 								</p>
// 							</div>
// 						</div>
// 					</div>
// 				</header>
// 				<div className="container-fluid blog-section">
// 					<div className="row">
// 						<div className="col">
// 							{this.renderList()}
// 						</div>
// 					</div>
// 				</div>
// 			</Fragment>
// 		);
// 	}
// };

const mapStateToProps = (state) => {
	console.log(state);
	return {
		blogs: Object.values(state.blogs),
	};
};

export default connect(mapStateToProps, { fetchBlogs })(BlogList);
