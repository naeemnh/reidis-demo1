import React from 'react';
import { Link } from 'react-router-dom';

const AboutSection = () => {
	return (
		<div className="container-fluid default-content">
			<div className="row">
				<div className="col">
					<div className="lg-text">
						<span>A YOUNG TEAM - DRIVEN</span>
						<br />
						<span className="other-color">INTERACTIVE AGENCY</span>
						<br />
						<span>BASED IN DUBAI</span>
					</div>
					<div className="normal-text">
						<p>
							Hello, world! We are Reidis, a new age Interacctive agency that
							brings brand custodians, gifted designers, creative developers,
							and imaginative writers from across the world with a unified
							vision to build brands with global resonance. At Reidis, we don't
							just provide solutions across mediums but deliver thought
							processes that integrate branding and marketing to ensure maximum
							visibility and conversions.
						</p>
					</div>
					<div className="btn-holder">
						<Link to="/about" className="cr-btn primary">
							more about us
						</Link>
					</div>
				</div>
			</div>
		</div>
	);
};

const ClientSection = () => {
	return (
		<div className="container-fluid clients-section">
			<div className="row">
				<div className="col">
					<div className="lg-text">
						<span>DELIGHTING OUR</span>
						<br />
						<span>CLIENTS IS OUR</span>
						<br />
						<span>MISSION.</span>
					</div>
					<div className="normal-text">
						<p>
							But I must explain to you how all this mistaken idea of denouncing
							pleasure and praising
							<br />
							pain was born and I will give you a complete account of the
							system.
						</p>
					</div>
					{/* ----------------------------------------------------------
					Have to make back end to dynamically add more client logos
                    ---------------------------------------------------------- */}
					<div className="clients-logos">
						<div className="logo-holder">
							<img src="images/client1.png" alt="" />
						</div>
						<div className="logo-holder">
							<img src="images/client2.png" alt="" />
						</div>
						<div className="logo-holder">
							<img src="images/client3.png" alt="" />
						</div>
						<div className="logo-holder">
							<img src="images/client4.png" alt="" />
						</div>
						<div className="logo-holder">
							<img src="images/client5.png" alt="" />
						</div>
						<div className="logo-holder">
							<img src="images/client6.png" alt="" />
						</div>
						<div className="logo-holder">
							<img src="images/client7.png" alt="" />
						</div>
						<div className="logo-holder">
							<img src="images/client8.png" alt="" />
						</div>
					</div>
				</div>
			</div>
		</div>
	);
};

const ContactSection = () => {
	return (
		<div className="container-fluid other-content">
			<div className="row">
				<div className="col">
					<div className="lg-text">
						have a project
						<br />
						for us?
					</div>
					<div className="normal-text">
						<p>
							Contact us and we’ll send you the brief form to fill.
							<br />
							Then we’ll contact you within 24 hours.
						</p>
					</div>
					<div className="btn-holder">
						<Link to="/contact" className="cr-btn ex-padding">
							Contact us
						</Link>
					</div>
				</div>
			</div>
		</div>
	);
};

const TeamSection = () => {
	const teamMembers = [
		{
			member: 'Naeem',
			image: 'images/naeem.png',
		},
		{
			member: 'Jithin',
			image: 'images/jithin.png',
		},
		{
			member: 'Catherine',
			image: 'images/catherine.png',
		},
		{
			member: 'Athul',
			image: 'images/athul.png',
		},
		{
			member: 'Subin',
			image: 'images/subin.png',
		},
		{
			member: 'Kriti',
			image: 'images/kriti.png',
		},
	];

	const loadTeamMembers = teamMembers.map((person) => {
		return (
			<div className="photo-holder" key={person.member}>
				<img src={person.image} alt="" />
				<p>{person.member}</p>
			</div>
		);
	});

	return (
		<div className="container-fluid team-section padding-for-team">
			<div className="row">
				<div className="col">
					<div className="extra-lg-text">
						<span>behind every</span>
						<br />
						<span>great project,</span>
						<br />
						<span>reidis team!</span>
					</div>
				</div>
			</div>
			<div className="team-photos">
				<div className="team-photos-holder">{loadTeamMembers}</div>
			</div>
		</div>
	);
};

export { AboutSection, ClientSection, ContactSection, TeamSection };
