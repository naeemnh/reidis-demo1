import React, { Fragment, useEffect } from 'react';
import { Router, Route, Switch } from 'react-router-dom';

import About from './About';
import Home from './Home';
import Blog from './Blog';
import BlogList from './BlogList';
import WorksList from './WorksList';
import Project from './Project';
import Services from './Services';
import Contact from './Contact';
import Header from './Header';
import Footer from './Footer';
import PageNotFound from './PageNotFound';
import history from '../history';
import FooterB from './FooterB';

const App = () => {
	useEffect(() => {
		window.scrollTo(0, 0);
	});
	return (
		<Fragment>
			<Router history={history}>
				<Fragment>
					<Header />
					<Switch>
						<Route path="/" exact component={Home} />
						<Route path="/about" exact component={About} />
						<Route path="/works" exact component={WorksList} />
						<Route path="/works/:id" exact component={Project} />
						<Route path="/services/:id" exact component={Services} />
						<Route path="/blog" exact component={BlogList} />
						<Route path="/blog/:id" exact component={Blog} />
						<Route path="/contact" exact component={Contact} />
						<Route path="/footer" exact component={FooterB} />
						<Route path="/" component={PageNotFound} />
					</Switch>
					<FooterB />
				</Fragment>
			</Router>
		</Fragment>
	);
};

export default App;
