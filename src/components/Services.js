import React, { Component, Fragment, useEffect } from 'react';
import { connect } from 'react-redux';
import { fetchService, fetchServices } from '../actions';
import Accordian from './Accordian';
import FeaturedImage from './FeaturedImage';

// con

class Services extends Component {
	componentDidMount() {
		const { id } = this.props.match.params;
		this.props.fetchServices();
		this.props.fetchService(id);
	}

	renderList() {
		const { service } = this.props;
		if (service) {
			const list = service.acf.list.split(', ');
			return list.map((item, index) => {
				return <li key={index}>{item}</li>;
			});
		}
	}

	render() {
		const { service } = this.props;
		if (service) console.log(service);
		return (
			<Fragment>
				<header className="container-fluid header service">
					<div className="row">
						<div className="col">
							<div className="extra-lg-text">
								<span className="other-color">
									{service ? service.title.rendered : null}
								</span>
							</div>
							<div className="normal-text">
								<p
									dangerouslySetInnerHTML={{
										__html: service ? service.content.rendered : null,
									}}></p>
							</div>
						</div>
					</div>
				</header>
				<div className="container-fluid service-info">
					<div className="container">
						{/* <div className="row">
							<div class="container">
							</div>
						</div> */}
						<div className="row">
							<div className="col-md-6">
								<div className="boxy">
									<div className="row">
										{service && service.acf.heading_1 ? (
											<h1 className="title">{service.acf.heading_1}</h1>
										) : null}
										<div className="normal-text">
											<p
												dangerouslySetInnerHTML={{
													__html: service ? service.acf.paragraph_1 : null,
												}}>
												{/* {service ? service.acf.paragraph_1 : null} */}
											</p>
										</div>
									</div>
								</div>
							</div>
							<div className="col-md-6">
								<div className="boxy">
									<div className="row">
										{service && service.acf.heading_2 ? (
											<h1 className="title">{service.acf.heading_2}</h1>
										) : null}
										<div className="normal-text">
											<p
												dangerouslySetInnerHTML={{
													__html: service ? service.acf.paragraph_2 : null,
												}}>
												{/* {service ? service.acf.paragraph_2 : null} */}
											</p>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div className="container-fluid service-info">
					<div className="row">
						<div className="col-md-8">
							<div className="boxy img-box">
								<div className="img">
									<FeaturedImage
										id={service && service > 0 ? service.featured_media : null}
									/>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div className="container-fluid service-info">
					<div className="container">
						<div className="row">
							<div className="col-md-6">
								<div className="boxy">
									<div className="row">
										{service && service.acf.heading_3 ? (
											<h1 className="title">{service.acf.heading_3}</h1>
										) : null}
										<div className="normal-text">
											<p
												dangerouslySetInnerHTML={{
													__html: service ? service.acf.paragraph_3 : null,
												}}>
												{/* {service ? service.acf.paragraph_3 : null} */}
											</p>
										</div>
									</div>
								</div>
							</div>
							<div className="col-md-6">
								<div className="boxy">
									<div className="row">
										{service && !service.acf.list ? (
											service && service.acf.heading_4 ? (
												<h1 className="title">{service.acf.heading_4}</h1>
											) : null
										) : null}

										<div className="normal-text">
											{service && !service.acf.list ? (
												<p
													dangerouslySetInnerHTML={{
														__html: service ? service.acf.paragraph_4 : null,
													}}>
													{/* {service ? service.acf.paragraph_4 : null} */}
												</p>
											) : (
												<ul>{this.renderList()}</ul>
											)}
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div className="container-fluid service-info">
					<div className="container">
						<div className="row">
							<div className="col">
								<div className="boxy">
									<div className="row flex-column">
										<p>Still in doubt? Normal!</p>
										<p>learn more</p>
										<h1 className="title">about our work</h1>
									</div>
									<Accordian />
								</div>
							</div>
						</div>
					</div>
				</div>
				<div className="container-fluid clients-section explore-section">
					<div className="row">
						<div className="col">
							<div className="lg-text">
								have a project
								<br />
								for us?
							</div>
							<div className="normal-text">
								<p>
									Contact us and we’ll send you the brief form to fill.
									<br />
									Then we’ll contact you within 24 hours.
								</p>
							</div>
							<div className="btn-holder">
								<a href="/works" className="cr-btn black ex-padding">
									Explore our works
								</a>
							</div>
						</div>
					</div>
				</div>
			</Fragment>
		);
	}
}

const mapStateToProps = (state, ownProps) => {
	console.log(state);
	return {
		service: state.services[ownProps.match.params.id],
		services: Object.values(state.services),
	};
};

export default connect(mapStateToProps, { fetchService, fetchServices })(
	Services
);
