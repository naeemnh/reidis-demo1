import React, { Fragment } from 'react';
// import { Link } from 'react-router-dom';

const Header = () => {
	return (
		<Fragment>
			<div className="menu-toggle">
				<div className="icon"></div>
			</div>
			<div className="main-menu">
				<div className="contant-info">
					<div>
						<a href="mailto:team@reidis.io">
							<span
								className="__cf_email__"
								data-cfemail="b2dad7dededdf2d1c0d78a9cd1dddf">
								team@reidis.io
							</span>
						</a>
					</div>
					<div>+971 52 727 5642</div>
				</div>
				<div className="menu-links">
					<ul>
						<li>
							<a href="/">Home</a>
						</li>
						<li>
							<a href="/about">About</a>
						</li>
						<li>
							<a href="/works">Work</a>
						</li>
						<li className="dropdown">
							<button>Services</button>
							<ul className="dropdown-menu">
								<li>
									<a className="dropdown-item" href="/services/212">
										Branding
									</a>
								</li>
								<li>
									<a className="dropdown-item" href="/services/220">
										Website Development
									</a>
								</li>
								<li>
									<a className="dropdown-item" href="/services/264">
										E-Commerce Integration
									</a>
								</li>
								<li>
									<a className="dropdown-item" href="/services/255">
										Digital Marketing
									</a>
								</li>
								<li>
									<a className="dropdown-item" href="/services/225">
										Application Development
									</a>
								</li>
								<li>
									<a className="dropdown-item" href="/services/267">
										Photo & Video Shooting
									</a>
								</li>
							</ul>
						</li>
						<li>
							<a href="/blog">Blog</a>
						</li>
						<li>
							<a href="/contact">Contact</a>
						</li>
					</ul>
				</div>
				<div className="social-media">
					<div className="social-link-holder">
						<a href="https://www.facebook.com/reidisinteractive">Facebook</a>
					</div>
					<div className="social-link-holder">
						<a href="https://instagram.com/reidis_io?utm_medium=copy_link">
							Instagram
						</a>
					</div>
					<div className="social-link-holder">
						<a href="https://www.linkedin.com/company/reidis-interactive/mycompany/">
							LinkedIn
						</a>
					</div>
					<div className="social-link-holder">
						<a href="https://www.behance.net/reidisinteractive">Behance</a>
					</div>
				</div>
			</div>
			<nav className="container-fluid cnav">
				<div className="row">
					<div className="col">
						<div className="logo-holder">
							<a href="/">
								<img className="logo" src="/images/r_icon2.jpg" alt="Reidis" />
							</a>
						</div>
					</div>
					<div className="col text-right">
						<div className="social-media">
							<div className="social-link-holder">
								<a
									target="_blank"
									rel="noreferrer"
									href="https://www.facebook.com/reidisinteractive">
									Facebook
								</a>
							</div>
							<div className="social-link-holder">
								<a
									target="_blank"
									rel="noreferrer"
									href="https://instagram.com/reidis_io?utm_medium=copy_link">
									Instagram
								</a>
							</div>
							<div className="social-link-holder">
								<a href="https://www.linkedin.com/company/reidis-interactive/mycompany/">
									LinkedIn
								</a>
							</div>
							<div className="social-link-holder">
								<a href="https://www.behance.net/reidisinteractive">Behance</a>
							</div>
						</div>
					</div>
				</div>
			</nav>
		</Fragment>
	);
};

export default Header;
